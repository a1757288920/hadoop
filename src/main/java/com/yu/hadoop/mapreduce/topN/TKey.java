package com.yu.hadoop.mapreduce.topN;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * 自定义类型必须实现接口：  序列化/反序列化   比较器
 * @author 26572
 */
public class TKey implements WritableComparable<TKey> {

    private int year;
    private int month;
    private int day;
    private int temperature;

    private String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    @Override
    public int compareTo(TKey that) {
        //我们为了让这个案例体现API开发，所以下面的逻辑是一种通用的逻辑，按照时间正序
        //但是目前业务需要的是   年，月，温度  且  温度倒序  ，所以还需要开发一个sortComparator
        int c1 = Integer.compare(this.year, that.getYear());
        //the value 0 if x == y; a value less than 0 if x < y; and a value greater than 0 if x > y
        if (c1 == 0){
            int c2 = Integer.compare(this.month, that.getMonth());
            if (c2 == 0){
                return Integer.compare(this.day,that.getDay());
            }
            return c2;
        }
        return c1;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(year);
        out.writeInt(month);
        out.writeInt(day);
        out.writeInt(temperature);
        out.writeUTF(location);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.year = in.readInt();
        this.month = in.readInt();
        this.day = in.readInt();
        this.temperature = in.readInt();
        this.location = in.readUTF();
    }
}
