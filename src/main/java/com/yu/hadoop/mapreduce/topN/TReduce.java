package com.yu.hadoop.mapreduce.topN;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

/**
 * @author 26572
 */
public class TReduce extends Reducer<TKey, IntWritable, Text,IntWritable> {

    Text rKey = new Text();
    IntWritable rValue = new IntWritable();

    @Override
    protected void reduce(TKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        //2019-6-1     33
        //2019-6-1     32
        //2019-6-22    31
        //2019-6-1     22
        //2019-6-1     31
        //问题：对着value进行迭代，key会不会变？
        //程序开发，方法传参的两种形式：值传递，引用传递
        Iterator<IntWritable> iter = values.iterator();
        int flag = 0;
        int day = 0;
        while (iter.hasNext()){
            IntWritable value = iter.next();
            // ->context.nextKeyValue()   ->   对key和value更新值
            if (flag == 0){
                rKey.set(key.getYear()+"-"+key.getMonth()+"-"+key.getDay()+"@"+key.getLocation());
                rValue.set(key.getTemperature());
                context.write(rKey,rValue);
                flag++;
                day = key.getDay();
            }
            if (flag != 0 && day != key.getDay()){
                rKey.set(key.getYear()+"-"+key.getMonth()+"-"+key.getDay()+"@"+key.getLocation());
                rValue.set(key.getTemperature());
                context.write(rKey,rValue);
                break;
            }
        }
        super.reduce(key, values, context);
    }
}
