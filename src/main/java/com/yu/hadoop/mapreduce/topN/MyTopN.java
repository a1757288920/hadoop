package com.yu.hadoop.mapreduce.topN;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

/**
 * 客户端
 * @author 26572
 */
public class MyTopN {

    public static void main(String[] args) throws Exception {

        //配置
        Configuration conf = new Configuration(true);
        conf.set("mapreduce.framework.name","local");
        conf.set("mapreduce.app-submission.cross-platform","true");

        //用工具类获取运行时参数
        String[] other = new GenericOptionsParser(conf, args).getRemainingArgs();
        //获取作业
        Job job = Job.getInstance(conf);
        job.setJarByClass(MyTopN.class);
        job.setJobName("TopN");

        //客户端规划的时候将join的右表cache到mapTask出现的节点上
        job.addCacheArchive(new Path("/data/topn/dict/dict.txt").toUri());

        /**
         * 初学者，关注的是client端代码梳理
         * 因为这块写明白了，也就知道作业的开发原理了
         */

    //mapTask
        //input and output
        TextInputFormat.addInputPath(job,new Path(other[0]));
        Path outPath = new Path(other[1]);
        if (outPath.getFileSystem(conf).exists(outPath)){
            outPath.getFileSystem(conf).delete(outPath,true);
        }
        TextOutputFormat.setOutputPath(job,outPath);
        //key
        //map
        job.setMapperClass(TMapper.class);
        job.setMapOutputKeyClass(TKey.class);
        job.setMapOutputValueClass(IntWritable.class);
        //partitioner   按 年、月  分区   ->   分区 > 分组       也可以按  年  分区
        //分区器 ： 只需要满足相同的key获得相同的分区号就OK！
        job.setPartitionerClass(TPartitioner.class);
        //sortComparator
        job.setSortComparatorClass(TSortComparator.class);
        //combine
//        job.setCombinerClass();

    //reduceTask
        //groupingComparator
        job.setGroupingComparatorClass(TGroupingComparator.class);
        //reduce
        job.setReducerClass(TReduce.class);

        job.waitForCompletion(true);
    }

}
