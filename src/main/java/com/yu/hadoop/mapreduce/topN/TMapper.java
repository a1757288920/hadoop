package com.yu.hadoop.mapreduce.topN;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * @author 26572
 */
public class TMapper extends Mapper<LongWritable, Text,TKey, IntWritable> {

    /**
     * 因为map可能被重复调起，创建对象放在方法外，减少GC
     * 同时，map输出的key，value，会发生序列化，变成字节数组进入buffer的
     */
    TKey mKey = new TKey();
    IntWritable mValue = new IntWritable();

    public HashMap<String,String> dict = new HashMap<String,String>();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        URI[] files = context.getCacheFiles();
        Path path = new Path(files[0].getPath());

        BufferedReader reader = new BufferedReader(new FileReader(new File(path.getName())));

        String line = reader.readLine();

        while (line != null){
            String[] split = line.split("\t");
            dict.put(split[0],split[1]);
            line = reader.readLine();
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //开发习惯：不要过于自信
        //从数据中拿出来一到两条
        //2019-6-1 22：22：22   1   31
        //2019-5-21 22：22：22  3   33
        String[] strs = StringUtils.split(value.toString(), '\t');
        //2019-6-1 22：22：22  |  1  |  31
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date= sdf.parse(strs[0]);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            mKey.setYear(calendar.get(Calendar.YEAR));
            mKey.setMonth(calendar.get(Calendar .MONTH) + 1);
            mKey.setDay(calendar.get(Calendar.DAY_OF_MONTH));
            int temperature = Integer.parseInt(strs[2]);
            mKey.setTemperature(temperature);
            mKey.setLocation(dict.get(strs[1]));
            mValue.set(temperature);

            context.write(mKey,mValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
