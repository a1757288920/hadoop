package com.yu.hadoop.mapreduce.topN;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;


/**
 * partitioner   按 年、月  分区   ->   分区 > 分组       也可以按  年  分区
 * 分区器 ： 只需要满足相同的key获得相同的分区号就OK！
 * @author 26572
 */
public class TPartitioner extends Partitioner<TKey, IntWritable> {
    @Override
    public int getPartition(TKey key, IntWritable value, int numPartitions) {
        //不能太复杂
        //数据倾斜
        return key.getYear() % numPartitions;

    }
}
