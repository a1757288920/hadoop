package com.yu.hadoop.mapreduce.fof;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class FReduce extends Reducer<Text, IntWritable,Text,IntWritable> {

    IntWritable rValue = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        //马老师 一明老师 0
        //马老师 一明老师 1
        //马老师 一明老师 3
        //马老师 一明老师 0
        //马老师 一明老师 1
        //马老师 一明老师 2

        int flag = 0;
        int sum = 0;
        for (IntWritable value : values) {
            if (value.get() == 0){
                flag = 1;
            }
            sum += value.get();
        }
        if (flag == 0){
            rValue.set(sum);
            context.write(key,rValue);
        }
    }
}
