package com.yu.hadoop.mapreduce.fof;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author 26572
 */
public class FMapper extends Mapper<LongWritable, Text,Text, IntWritable> {

    Text mKey = new Text();
    IntWritable mValue = new IntWritable();

    /**
     * 马老师 一明老师 刚老师 周老师
     */
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] strs = StringUtils.split(value.toString(), ' ');

        for (int i = 1; i < strs.length; i++) {
            mKey.set(getFof(strs[0],strs[i]));
            mValue.set(0);
            context.write(mKey,mValue);
            for (int j = i + 1; j < strs.length; j++) {
                mKey.set(getFof(strs[i],strs[j]));
                mValue.set(1);
                context.write(mKey,mValue);
            }
        }
    }

    public static String getFof(String str1,String str2){
        if (str1.compareTo(str2) > 0){
            return str1 + " - " + str2;
        }else {
            return str2 + " - " + str1;
        }
    }
}
