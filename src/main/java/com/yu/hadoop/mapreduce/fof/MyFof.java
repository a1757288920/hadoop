package com.yu.hadoop.mapreduce.fof;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 * @author 26572
 */
public class MyFof {

    public static void main(String[] args) throws Exception {

        //配置
        Configuration conf = new Configuration(true);
        conf.set("mapreduce.framework.name","local");
        conf.set("mapreduce.app-submission.cross-platform","true");

        //用工具类获取运行时参数
        String[] other = new GenericOptionsParser(conf, args).getRemainingArgs();
        //获取作业
        Job job = Job.getInstance(conf);
        job.setJarByClass(MyFof.class);
        job.setJobName("TopN");

        /**
         * 初学者，关注的是client端代码梳理
         * 因为这块写明白了，也就知道作业的开发原理了
         */

        //mapTask
        //input and output
        TextInputFormat.addInputPath(job,new Path(other[0]));
        Path outPath = new Path(other[1]);
        if (outPath.getFileSystem(conf).exists(outPath)){
            outPath.getFileSystem(conf).delete(outPath,true);
        }
        TextOutputFormat.setOutputPath(job,outPath);
        //key
        //map
        job.setMapperClass(FMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //reduceTask
        //reduce
        job.setReducerClass(FReduce.class);

        job.waitForCompletion(true);
    }

}
