package com.yu.hadoop.mapreduce.wc;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class MyMapper extends Mapper<Object, Text,Text, IntWritable> {
    /**
     * 1、hadoop框架，是一个分布式的框架，   数据一定伴随着序列化与反序列化
     * hadoop有一套自己的可序列化，反序列化的类型
     * 如果有特殊需求可以自己开发类型，但必须实现序列化和反序列化接口
     * 2、计算时一定伴随着排序
     * 排序：字典序、数值序
     * 所以自己开发类型时，还需要实现比较器
     */
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key,Text value,Context context) throws IOException, InterruptedException {
        StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.hasMoreTokens()){
            word.set(itr.nextToken());
            context.write(word,one);
        }
    }
}
