package com.yu.hadoop.mapreduce.wc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class MyWordCount {

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration(true);

        GenericOptionsParser genericOptionsParser = new GenericOptionsParser(conf,args);
        String[] remainingArgs = genericOptionsParser.getRemainingArgs();

        conf.set("mapreduce.app-submission.cross-platform","true");
        Job job = Job.getInstance(conf);

        job.setJarByClass(MyWordCount.class);

        job.setJar("G:\\hadoop-hdfs\\target\\hadoop-hdfs-1.0.jar");

        job.setJobName("yu");

        Path infile = new Path("/data/wc/input");
        TextInputFormat.addInputPath(job,infile);
        Path outfile = new Path("data/wc/output");
        if (outfile.getFileSystem(conf).exists(outfile)){
            outfile.getFileSystem(conf).delete(outfile,true);
        }
        TextOutputFormat.setOutputPath(job,outfile);

        job.setMapperClass(MyMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);
        job.setReducerClass(MyReducer.class);

        job.waitForCompletion(true);
        /**
         * <p>Here is an example on how to submit a job:</p>
         * <p><blockquote><pre>
         *     // Create a new Job
         *     Job job = Job.getInstance();
         *     job.setJarByClass(MyJob.class);
         *
         *     // Specify various job-specific parameters
         *     job.setJobName("myjob");
         *
         *     job.setInputPath(new Path("in"));
         *     job.setOutputPath(new Path("out"));
         *
         *     job.setMapperClass(MyJob.MyMapper.class);
         *     job.setReducerClass(MyJob.MyReducer.class);
         *
         *     // Submit the job, then poll for progress until the job is complete
         *     job.waitForCompletion(true);
         * </pre></blockquote></p>
         */


    }

}
