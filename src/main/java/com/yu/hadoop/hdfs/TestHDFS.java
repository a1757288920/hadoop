package com.yu.hadoop.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.net.URI;

public class TestHDFS {

    public Configuration configuration = null;
    public FileSystem fileSystem = null;

    //C/S
    @Before
    public void connection() throws Exception {
        //true  加载配置文件
        configuration = new Configuration(true);
//        fileSystem = FileSystem.get(configuration);
        fileSystem = FileSystem.get(URI.create("hdfs://mycluster/"),configuration,"god");
    }

    @Test
    public void mkdir() throws Exception {
        Path dir = new Path("/yxf01");
        if (fileSystem.exists(dir)){
            fileSystem.delete(dir,true);
        }
        fileSystem.mkdirs(dir);
    }

    @Test
    public void upload() throws Exception {
        BufferedInputStream input = new BufferedInputStream(new FileInputStream(new File("./data/hello.txt")));
        Path outfile = new Path("/yxf/out.txt");
        FSDataOutputStream output = fileSystem.create(outfile);

        IOUtils.copyBytes(input,output,configuration,true);

    }

    @Test
    public void blocks() throws Exception {

        Path file = new Path("/user/god/data.text");
        FileStatus fileStatus = fileSystem.getFileStatus(file);
        BlockLocation[] fileBlockLocations = fileSystem.getFileBlockLocations(fileStatus, 0, fileStatus.getLen());
        for (BlockLocation b :
                fileBlockLocations) {
            System.out.println(b);
        }

//          0,1048576,localhost,localhost
//          1048576,1048576,localhost,localhost
//        当计算向数据移动后，用户和程序读取的都是文件这个级别，并不知道有块这个概念
//        面向文件打开的输入流，无论怎么读都是从文件开始读起

//        计算向数据移动后，期望的是分治，只读取自己关心（通过seek实现），同时，具备距离的概念（优先和本地的DN获取数据---框架的默认机制）
        FSDataInputStream inputStream = fileSystem.open(file);

        inputStream.seek(1048576);
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
        System.out.println((char)inputStream.readByte());
    }

    @After
    public void close() throws Exception {
        fileSystem.close();
    }

}
